<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
<h1>Пример работы с WebSocket</h1>
<div class="comnat">
    <ul>
        <li><a href="/?name=1">Камната 1</a></li>
        <li><a href="/?name=2">Камната 2</a></li>
        <li><a href="/?name=3">Камната 3</a></li>
        <li><a href="/?name=4">Камната 4</a></li>
    </ul>
</div>
<form action="" name="messages">
    <div class="row">Имя: <input type="text" name="name"></div>
    <div class="row">Текст: <input type="text" name="text"></div>
    <div class="row"><input type="submit" value="Поехали"></div>
</form>
<div id="status"></div>
<script>
    window.onload = function(){
        var com = "<?=$_GET['name']?>";
        var socket = new WebSocket("ws://localhost:8000");
        var status = document.querySelector("#status");

        socket.onopen = function() {
            status.innerHTML = "cоединение установлено";
        };

        socket.onclose = function(event) {
            if (event.wasClean) {
                status.innerHTML = 'cоединение закрыто';
            } else {
                status.innerHTML = 'соединения как-то закрыто';
            }
            status.innerHTML += '<br>код: ' + event.code + ' причина: ' + event.reason;
        };

        socket.onmessage = function(event) {
            var array = JSON.parse(event.data);
            if(com == array.com){
                status.innerHTML += "" +
                    "<div>" +
                        "<div><b>" + array.name + "</b></div>" +
                        "<div>" + array.text + "</div>" +
                    "</div>";
            }

        };

        socket.onerror = function(event) {
            status.innerHTML = "ошибка " + event.message;
        };

        document.forms["messages"].onsubmit = function () {
            var name = this.name.value;
            var text = this.text.value;
            if(name != '' && text != ''){
                var send = JSON.stringify({
                    com: com,
                    name: name,
                    text: text
                });
                socket.send(send);
            }

            return false;
        }
    }
</script>
<?php
phpinfo();
?>
</body>
</html>